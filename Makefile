tests := $(patsubst src/test/c/%Test.c,tests/%.test,$(wildcard src/test/c/*.c))

QEMU_EXISTS := $(shell [ -e qemu-system-x86_64 ] && echo 1 || echo 0)

check: $(tests)
ifneq ($(QEMU_EXISTS),0) 
	$(error 'QEMU Not Installed!')
endif
	$(tests)

tests/%.test: tests/%Test.o munit/munit.o
	mkdir -p tests
	cp Core-current.iso tests/Core-current.iso
	gcc -o $@ tests/munit.o $<

tests/%.o: src/test/c/%.c
	mkdir -p tests
	gcc -c $< -o $@

munit/munit.o: munit/munit.c
	mkdir -p tests
	gcc -c munit/munit.c -o tests/munit.o

clean: 
	rm -f *.o & rm -f *.test
	rm -rf tests

.PHONY: clean check
