The OS Project
--

We will build an OS from the ground up.  It will provide a homogenous view to all code and data. It will be fast - and provide a basis for web applications and other useful purposes.

**Required Software**

1. QEMU - A commandline virtual machine host.

`brew install qemu`

