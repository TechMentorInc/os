#!/bin/bash

memsave='pmemsave 0xb8000 1024 "/dev/stdout"'

(echo -n; sleep 5; echo $memsave; echo 'quit') | (qemu-system-x86_64 -monitor stdio -serial /dev/null  -m 2G --drive media=cdrom,file=Core-current.iso) | hexdump -Cv
