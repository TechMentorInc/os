#include <stdio.h>
#include <unistd.h>
#include <fcntl.h>
#include <errno.h>
#include "munit.h"

MunitResult boottest(const MunitParameter params[], void* user_data_or_fixture);

MunitTest tests[] = {
  {
    "/boot-test/initial", /* name */
    boottest, /* test */
    NULL, /* setup */
    NULL, /* tear_down */
    MUNIT_TEST_OPTION_NONE, /* options */
    NULL /* parameters */
  },
  /* Mark the end of the array with an entry where the test
   * function is NULL */
  { NULL, NULL, NULL, NULL, MUNIT_TEST_OPTION_NONE, NULL }
};

static const MunitSuite suite = {
  "/boot-tests", /* name */
  tests, /* tests */
  NULL, /* suites */
  1, /* iterations */
  MUNIT_SUITE_OPTION_NONE /* options */
};

MunitResult boottest(const MunitParameter params[], void* user_data_or_fixture){ 
    int fdout[2];
    int fdin[2];
    char memsave[]="pmemsave 0xb8000 1024 \"/dev/stdout\"\n";
    char quit[]="quit\n";
    unsigned char *buf=malloc(4000);
    char *s=malloc(4000);
    int result;    
    int i,m;

    pipe(fdout);
    pipe(fdin);

    switch(fork()){
        case -1: 
            printf("Fork failure\n");
            break;
        case 0:
            close(fdout[0]);
            close(fdin[1]);
            dup2(fdout[1], STDOUT_FILENO);
            dup2(fdin[0], STDIN_FILENO);
            
            result=execlp(
                "qemu-system-x86_64", 
                "qemu-system-x86_64", 
                "-monitor", "stdio", 
                "-serial", "/dev/null",  
                "-m", "2G", 
                "--drive", "media=cdrom,file=tests/Core-current.iso",NULL);
            break;
        default:
            close(fdout[1]);
            close(fdin[0]);
            sleep(2);

            write(fdin[1],memsave,sizeof(memsave));
            write(fdin[1],quit,sizeof(quit));
            close(fdin[1]);
            sleep(2);

            read(fdout[0],buf,4000);
            for(m=0,i=0; i<4000; i++)
            {
                if(buf[i]>=20 && buf[i]<=126)
                {
                    s[m] = buf[i];
                    m++;
                }   
            }
            munit_assert_not_null(strstr(s,"www.tinycorelinux.net"));
            break;
    }
    return MUNIT_OK;
}

int main (int argc, char** argv) {
  return munit_suite_main(&suite, NULL, argc, argv);
}
